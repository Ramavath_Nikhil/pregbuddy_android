package com.dtq.pregbuddy.Base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dtq.pregbuddy.CustomViews.CustomProgressDialog;

import butterknife.ButterKnife;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {


    private CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResource());
        ButterKnife.bind(this);
        init(savedInstanceState);
    }


    /**
     * Layout resource to be inflated
     *
     * @return layout resource
     */
    @LayoutRes
    protected abstract int getContentResource();

    /**
     * Initialisations
     */
    protected abstract void init(@Nullable Bundle state);


    public void showProgressDialog() {
        customProgressDialog = new CustomProgressDialog(BaseActivity.this);
        customProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (customProgressDialog != null)
            customProgressDialog.cancel();
    }
}