package com.dtq.pregbuddy.Retrofit;

import com.dtq.pregbuddy.Topics.Models.CreateTopicRequest;
import com.dtq.pregbuddy.Topics.Models.GetTopicsResponse;
import com.dtq.pregbuddy.Topics.Models.PostActionRequest;
import com.dtq.pregbuddy.Topics.Models.PostActionResponse;
import com.dtq.pregbuddy.Topics.Models.Topic_;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public interface Api {

    @GET("gettopics/{user_name}")
    Call<GetTopicsResponse> getTopics(@Path("user_name") String user_name, @Query("page") int page, @Query("size") int size);

    @POST("postaction")
    Call<PostActionResponse> postAction(@Body PostActionRequest postActionRequest);

    @POST("createtopic")
    Call<Topic_> createTopic(@Body CreateTopicRequest CreateTopicRequest);
}

