package com.dtq.pregbuddy.CustomViews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.dtq.pregbuddy.R;


public class CustomProgressDialog extends Dialog {

    ProgressBar progressView;

    public CustomProgressDialog(Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = (View) LayoutInflater.from(getContext()).inflate(R.layout.view_customprogressbar, null);
        setContentView(view);
        this.setCanceledOnTouchOutside(false);


    }

    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

}
