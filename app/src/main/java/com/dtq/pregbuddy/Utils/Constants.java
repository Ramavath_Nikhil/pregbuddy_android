package com.dtq.pregbuddy.Utils;

import retrofit2.http.PUT;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class Constants {

    //TOPICS SCREEN CONSTANTS
    public static final String BASE_URL = "https://c0424b6f.ngrok.io/";
    public static final int SUCCESS_CODE = 200;
    public static final int PAGINATION_SIZE = 10;
    public static final int ACTION_UPVOTE = 1;
    public static final int ACTION_DOWNVOTE = -1;


    //SPLASH SCREEN CONSTANTS
    public static final String USERNAME = "username";
}
