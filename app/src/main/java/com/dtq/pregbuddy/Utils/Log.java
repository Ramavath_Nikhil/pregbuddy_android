package com.dtq.pregbuddy.Utils;

import android.content.Context;
import android.widget.Toast;

import com.dtq.pregbuddy.BuildConfig;


/**
 * Created by nikhilramavath on 16/03/18.
 */

public class Log {

    public static void e(String key, String message) {
        if (BuildConfig.DEBUG)
            android.util.Log.e(key, message);
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
