package com.dtq.pregbuddy.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by nikhilramavath on 16/03/18.
 */

public class SharedPrefernce {

    private static SharedPrefernce mInstance;
    private SharedPreferences sharedPreferences;

    private SharedPrefernce() {

    }

    public static SharedPrefernce getInstance() {
        if (mInstance == null) mInstance = new SharedPrefernce();


        return mInstance;
    }

    public void initialize(Context mContext) {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putString(key, value);
        e.apply();
    }

    public String getString(String key) {

        return sharedPreferences.getString(key, "");
    }




}
