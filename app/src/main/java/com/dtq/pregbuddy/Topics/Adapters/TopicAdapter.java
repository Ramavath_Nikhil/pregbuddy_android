package com.dtq.pregbuddy.Topics.Adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dtq.pregbuddy.R;
import com.dtq.pregbuddy.Topics.Models.Topic;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicsViewHolder> {

    private List<Topic> topics;
    private TopicAdapterEvents topicAdapterEvents;

    public TopicAdapter(List<Topic> topics, TopicAdapterEvents topicAdapterEvents) {

        this.topics = topics;
        this.topicAdapterEvents = topicAdapterEvents;
    }


    @Override
    public TopicsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_topic, parent, false);

        return new TopicsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopicsViewHolder holder, int position) {

        Topic topic = topics.get(position);
        holder.tvTopic.setText(topic.getTopic().getTopicText());
        holder.tvAuthor.setText(topic.getTopic().getUserId());
        holder.tvUpVote.setText(String.valueOf(topic.getTopic().getUpVoteCount()));
        holder.tvDownVote.setText(String.valueOf(topic.getTopic().getDownVoteCount()));
        holder.ivUpVote.setEnabled(true);
        holder.ivDownVote.setEnabled(true);
        if (topic.getUpVoted())
            holder.ivUpVote.setColorFilter(ContextCompat.getColor(holder.ivUpVote.getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);
        else
            holder.ivUpVote.setColorFilter(ContextCompat.getColor(holder.ivUpVote.getContext(), android.R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);

        if (topic.getDownVoted())
            holder.ivDownVote.setColorFilter(ContextCompat.getColor(holder.ivUpVote.getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);
        else
            holder.ivDownVote.setColorFilter(ContextCompat.getColor(holder.ivUpVote.getContext(), android.R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);


    }


    @Override
    public int getItemCount() {
        return topics.size();
    }

    public class TopicsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTopic)
        TextView tvTopic;
        @BindView(R.id.tvAuthor)
        TextView tvAuthor;
        @BindView(R.id.ivUpvote)
        ImageView ivUpVote;
        @BindView(R.id.ivDownVote)
        ImageView ivDownVote;
        @BindView(R.id.tvDownVote)
        TextView tvDownVote;
        @BindView(R.id.tvUpVote)
        TextView tvUpVote;

        public TopicsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ivUpvote)
        public void onUpVote(View view) {
            view.setEnabled(false);
            topicAdapterEvents.onTopicUpVoted(getAdapterPosition());
        }

        @OnClick(R.id.ivDownVote)
        public void onDownVote(View view) {
            view.setEnabled(false);
            topicAdapterEvents.onTopicDownVoted(getAdapterPosition());
        }

    }

    public interface TopicAdapterEvents {
        void onTopicUpVoted(int adapterPosition);

        void onTopicDownVoted(int adapterPosition);
    }
}
