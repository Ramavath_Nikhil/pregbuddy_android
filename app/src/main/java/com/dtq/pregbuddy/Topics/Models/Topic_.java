
package com.dtq.pregbuddy.Topics.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topic_ {

    @SerializedName("topicText")
    @Expose
    private String topicText;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("upVoteCount")
    @Expose
    private Integer upVoteCount;
    @SerializedName("downVoteCount")
    @Expose
    private Integer downVoteCount;
    @SerializedName("id")
    @Expose
    private String id;

    public String getTopicText() {
        return topicText;
    }

    public void setTopicText(String topicText) {
        this.topicText = topicText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public Integer getUpVoteCount() {
        return upVoteCount;
    }

    public void setUpVoteCount(Integer upVoteCount) {
        this.upVoteCount = upVoteCount;
    }

    public Integer getDownVoteCount() {
        return downVoteCount;
    }

    public void setDownVoteCount(Integer downVoteCount) {
        this.downVoteCount = downVoteCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
