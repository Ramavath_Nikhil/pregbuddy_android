package com.dtq.pregbuddy.Topics.Models;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class PostActionResponse {

    private String userId;
    private String topicId;
    private int action;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    private String created;
}
