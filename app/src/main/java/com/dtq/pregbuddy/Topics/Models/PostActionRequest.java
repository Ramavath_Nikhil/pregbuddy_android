package com.dtq.pregbuddy.Topics.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class PostActionRequest {


    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("topicId")
    @Expose
    private String topicId;
    @SerializedName("action")
    @Expose
    private int action;

    public PostActionRequest(String userId, String topicId, int action) {
        this.userId = userId;
        this.topicId = topicId;
        this.action = action;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }


}
