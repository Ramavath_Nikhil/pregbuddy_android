package com.dtq.pregbuddy.Topics.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class CreateTopicRequest {


    @SerializedName("topicText")
    @Expose
    private String topicText;
    @SerializedName("userId")
    @Expose
    private String userId;

    public CreateTopicRequest(String topic, String userId) {
        this.topicText = topic;
        this.userId = userId;
    }

    public String getTopicText() {
        return topicText;
    }

    public void setTopicText(String topicText) {
        this.topicText = topicText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
