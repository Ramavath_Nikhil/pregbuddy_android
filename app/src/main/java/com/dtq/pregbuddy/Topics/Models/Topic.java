
package com.dtq.pregbuddy.Topics.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topic {

    @SerializedName("topic")
    @Expose
    private Topic_ topic;
    @SerializedName("upVoted")
    @Expose
    private Boolean upVoted;
    @SerializedName("downVoted")
    @Expose
    private Boolean downVoted;

    public Topic(Topic_ topic_) {
        this.topic = topic_;
        this.upVoted = false;
        this.downVoted = false;

    }

    public Topic_ getTopic() {
        return topic;
    }

    public void setTopic(Topic_ topic) {
        this.topic = topic;
    }

    public Boolean getUpVoted() {
        return upVoted;
    }

    public void setUpVoted(Boolean upVoted) {
        this.upVoted = upVoted;
    }

    public Boolean getDownVoted() {
        return downVoted;
    }

    public void setDownVoted(Boolean downVoted) {
        this.downVoted = downVoted;
    }

}
