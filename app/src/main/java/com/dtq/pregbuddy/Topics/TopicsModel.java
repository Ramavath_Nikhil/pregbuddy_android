package com.dtq.pregbuddy.Topics;

import android.widget.Toast;

import com.dtq.pregbuddy.Retrofit.RestClient;
import com.dtq.pregbuddy.Topics.Models.CreateTopicRequest;
import com.dtq.pregbuddy.Topics.Models.GetTopicsResponse;
import com.dtq.pregbuddy.Topics.Models.PostActionRequest;
import com.dtq.pregbuddy.Topics.Models.PostActionResponse;
import com.dtq.pregbuddy.Topics.Models.Topic;
import com.dtq.pregbuddy.Topics.Models.Topic_;
import com.dtq.pregbuddy.Utils.Constants;
import com.dtq.pregbuddy.Utils.SharedPrefernce;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class TopicsModel implements TopicsContract.model {

    private TopicsPresenter topicsPresenter;

    public TopicsModel(TopicsPresenter topicsPresenter) {

        this.topicsPresenter = topicsPresenter;
    }

    @Override
    public void getTopicsFromServer(int page, int size,boolean showProgress) {
        if(showProgress)
        topicsPresenter.getView().showProgressBar();
        new RestClient(Constants.BASE_URL).get().getTopics(SharedPrefernce.getInstance().getString(Constants.USERNAME), page, size).enqueue(new Callback<GetTopicsResponse>() {
            @Override
            public void onResponse(Call<GetTopicsResponse> call, Response<GetTopicsResponse> response) {
                topicsPresenter.getView().hideProgressBar();
                if (response.code() == Constants.SUCCESS_CODE) {
                    topicsPresenter.getView().onTopicsLoaded(response.body().getTopic());
                }
            }

            @Override
            public void onFailure(Call<GetTopicsResponse> call, Throwable t) {

                topicsPresenter.getView().showProgressBar();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void postAction(String userId, String topicId, final int action, final int adapterPosition) {
        new RestClient(Constants.BASE_URL).get().postAction(new PostActionRequest(userId, topicId, action)).enqueue(new Callback<PostActionResponse>() {
            @Override
            public void onResponse(Call<PostActionResponse> call, Response<PostActionResponse> response) {

                if (response.code() == Constants.SUCCESS_CODE) {
                    if (action == Constants.ACTION_UPVOTE) {
                        topicsPresenter.getView().onUpVoted(adapterPosition);
                    } else
                        topicsPresenter.getView().onDownVoted(adapterPosition);

                } else {
                    //CONSIDERING AS SUCCESS IN FAILURE CASE AS THERER IS NO INTERNAL DB
                    if (action == Constants.ACTION_UPVOTE) {
                        topicsPresenter.getView().onUpVoted(adapterPosition);
                    } else
                        topicsPresenter.getView().onDownVoted(adapterPosition);
                }
            }

            @Override
            public void onFailure(Call<PostActionResponse> call, Throwable t) {

                //CONSIDERING AS SUCCESS IN FAILURE CASE AS THERER IS NO INTERNAL DB
                if (action == Constants.ACTION_UPVOTE) {
                    topicsPresenter.getView().onUpVoted(adapterPosition);
                } else
                    topicsPresenter.getView().onDownVoted(adapterPosition);

            }
        });
    }

    @Override
    public void createTopicAPI(String topic, String userId) {
        topicsPresenter.getView().showProgressBar();
        new RestClient(Constants.BASE_URL).get().createTopic(new CreateTopicRequest(topic, userId)).enqueue(new Callback<Topic_>() {
            @Override
            public void onResponse(Call<Topic_> call, Response<Topic_> response) {

                topicsPresenter.getView().hideProgressBar();
                topicsPresenter.getView().onTopicCreated(new Topic(response.body()), true);

            }

            @Override
            public void onFailure(Call<Topic_> call, Throwable t) {
                topicsPresenter.getView().hideProgressBar();
                topicsPresenter.getView().onTopicCreated(null, false);

            }
        });
    }

}


