package com.dtq.pregbuddy.Topics;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.dtq.pregbuddy.Base.BaseActivity;
import com.dtq.pregbuddy.R;
import com.dtq.pregbuddy.Topics.Adapters.TopicAdapter;
import com.dtq.pregbuddy.Topics.Models.Topic;
import com.dtq.pregbuddy.Utils.Constants;
import com.dtq.pregbuddy.Utils.Log;
import com.dtq.pregbuddy.Utils.SharedPrefernce;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

public class TopicsActivity extends BaseActivity implements TopicsContract.View, TopicAdapter.TopicAdapterEvents {


    private int page = 0;
    @BindView(R.id.rvTopics)
    RecyclerView rvTopics;
    private TopicsPresenter topicsPresenter;
    private TopicAdapter topicAdapter;
    private List<Topic> topics = new ArrayList<>();
    private Dialog createTopicDialog = null;
    private boolean disablePagination = false;
    private boolean isReachedEnd;

    @Override
    protected int getContentResource() {
        return R.layout.activity_topics;
    }

    @Override
    protected void init(@Nullable Bundle state) {
        topicsPresenter = new TopicsPresenter();
        topicsPresenter.attach(this);
        topicsPresenter.loadTopics(TopicsActivity.this, page, true);
    }


    @Override
    public void onTopicsLoaded(List<Topic> topics) {

        this.topics.addAll(topics);
        if (topicAdapter == null) {
            topicAdapter = new TopicAdapter(this.topics, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rvTopics.setLayoutManager(mLayoutManager);
            rvTopics.setItemAnimator(new DefaultItemAnimator());
            rvTopics.setAdapter(topicAdapter);
            rvTopics.addOnScrollListener(onScrollListener);
        } else {
            topicAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onUpVoted(int adapterPosition) {

        Topic topic = topics.get(adapterPosition);
        topic.setUpVoted(true);
        topic.getTopic().setUpVoteCount(topic.getTopic().getUpVoteCount() + 1);
        topicAdapter.notifyItemChanged(adapterPosition);
    }

    @Override
    public void onDownVoted(int adapterPosition) {
        Topic topic = topics.get(adapterPosition);
        topic.setDownVoted(true);
        topic.getTopic().setDownVoteCount(topic.getTopic().getDownVoteCount() + 1);
        topicAdapter.notifyItemChanged(adapterPosition);
    }

    @Override
    public void onTopicCreated(Topic topic, boolean isCreated) {

        if (isCreated) {
            topics.add(topic);
            topicAdapter.notifyItemInserted(topics.size() - 1);
            disablePagination = true;
            rvTopics.scrollToPosition(topics.size() - 1);
            if (createTopicDialog != null && createTopicDialog.isShowing())
                createTopicDialog.cancel();
        } else
            Log.toast(TopicsActivity.this, getString(R.string.something_went_wrong));
    }

    @Override
    public void showProgressBar() {
        showProgressDialog();
    }

    @Override
    public void hideProgressBar() {

        hideProgressDialog();
    }


    @Override
    public void onTopicUpVoted(int adapterPosition) {

        Topic topic = topics.get(adapterPosition);
        topicsPresenter.upVoteTopic(SharedPrefernce.getInstance().getString(Constants.USERNAME), topic.getTopic().getId(), adapterPosition);

    }

    @Override
    public void onTopicDownVoted(int adapterPosition) {
        Topic topic = topics.get(adapterPosition);
        topicsPresenter.downVoteTopic(SharedPrefernce.getInstance().getString(Constants.USERNAME), topic.getTopic().getId(), adapterPosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_topic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_topic:

                showAddTopicDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddTopicDialog() {
        createTopicDialog = new Dialog(TopicsActivity.this);
        createTopicDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        createTopicDialog.setContentView(R.layout.layout_popup_add_topic);
        if (createTopicDialog.getWindow() != null)
            createTopicDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button bSubmit = createTopicDialog.findViewById(R.id.bSubmit);
        final EditText evTopic = createTopicDialog.findViewById(R.id.evTopic);

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (evTopic.getText().toString().isEmpty()) {
                    Log.toast(TopicsActivity.this, getString(R.string.please_enter_topic));
                } else {

                    topicsPresenter.createTopic(evTopic.getText().toString(), SharedPrefernce.getInstance().getString(Constants.USERNAME));
                }
            }
        });

        createTopicDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(createTopicDialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels - 50;


        createTopicDialog.getWindow().setLayout(width, height - 150);
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == SCROLL_STATE_IDLE && !recyclerView.canScrollVertically(1)) {
                if (!disablePagination) {
                    page = page + 1;
                    topicsPresenter.loadTopics(TopicsActivity.this, page, false);
                    disablePagination = false;
                }
            }
        }
    };
}