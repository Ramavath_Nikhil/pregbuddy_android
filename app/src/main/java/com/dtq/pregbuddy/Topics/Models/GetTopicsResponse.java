
package com.dtq.pregbuddy.Topics.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTopicsResponse {

    @SerializedName("topic")
    @Expose
    private List<Topic> topic = null;
    @SerializedName("pageable")
    @Expose
    private Pageable pageable;

    public List<Topic> getTopic() {
        return topic;
    }

    public void setTopic(List<Topic> topic) {
        this.topic = topic;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

}
