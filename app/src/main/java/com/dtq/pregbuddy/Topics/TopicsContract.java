package com.dtq.pregbuddy.Topics;

import android.content.Context;

import com.dtq.pregbuddy.Base.BaseMvpPresenter;
import com.dtq.pregbuddy.Base.BaseView;
import com.dtq.pregbuddy.Topics.Models.Topic;

import java.util.List;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public interface TopicsContract {
    // User actions. Presenter will implement
    interface Presenter extends BaseMvpPresenter<View> {
        void loadTopics(Context context, int page, boolean showProgress);

        void upVoteTopic(String userId, String topicId, int adapterPosition);

        void downVoteTopic(String userId, String topicId, int adapterPosition);

        void createTopic(String topic, String userId);
    }

    // Action callbacks. Activity/Fragment will implement
    interface View extends BaseView {
        void onTopicsLoaded(List<Topic> topics);

        void onUpVoted(int adapterPosition);

        void onDownVoted(int adapterPosition);

        void onTopicCreated(Topic topic,boolean isCreated);

        void showProgressBar();


        void hideProgressBar();
    }

    interface model extends BaseView {
        void getTopicsFromServer(int page, int size,boolean showProgress);

        void createTopicAPI(String topic, String userId);

        void postAction(String userId, String topicId, final int action, final int adapterPosition);
    }


}
