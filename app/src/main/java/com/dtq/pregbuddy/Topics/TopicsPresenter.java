package com.dtq.pregbuddy.Topics;

import android.content.Context;

import com.dtq.pregbuddy.Base.BasePresenter;
import com.dtq.pregbuddy.Utils.Constants;

/**
 * Created by nikhilramavath on 21/03/18.
 */

public class TopicsPresenter extends BasePresenter<TopicsContract.View> implements TopicsContract.Presenter {


    private TopicsModel topicsModel;

    public TopicsPresenter()

    {
        topicsModel = new TopicsModel(this);
    }

    @Override
    public void loadTopics(Context context, int page, boolean showProgress) {

        topicsModel.getTopicsFromServer(page, Constants.PAGINATION_SIZE,showProgress);
    }

    @Override
    public void upVoteTopic(String userId, String topicId, int adapterPosition) {

        topicsModel.postAction(userId, topicId, Constants.ACTION_UPVOTE, adapterPosition);
    }

    @Override
    public void downVoteTopic(String userId, String topicId, int adapterPosition) {
        topicsModel.postAction(userId, topicId, Constants.ACTION_DOWNVOTE, adapterPosition);
    }

    @Override
    public void createTopic(String topic, String userId) {
        topicsModel.createTopicAPI(topic, userId);

    }



}
