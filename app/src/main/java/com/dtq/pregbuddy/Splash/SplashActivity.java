package com.dtq.pregbuddy.Splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dtq.pregbuddy.R;
import com.dtq.pregbuddy.Topics.TopicsActivity;
import com.dtq.pregbuddy.Utils.Constants;
import com.dtq.pregbuddy.Utils.Log;
import com.dtq.pregbuddy.Utils.SharedPrefernce;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.evUserName)
    EditText evUserName;

    @BindView(R.id.bSubmit)
    Button bSubmit;

    @BindView(R.id.tvWelcomeMessage)
    TextView tvWelcomeMessage;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(getSupportActionBar()!=null)
            getSupportActionBar().hide();
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bSubmit)
    public void submitUserName() {
        if (evUserName.getText().toString().isEmpty())
            Log.toast(SplashActivity.this, getString(R.string.please_enter_your_name));
        else {

            SharedPrefernce.getInstance().putString(Constants.USERNAME, evUserName.getText().toString());
            startTopicsActivity();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPrefernce.getInstance().initialize(SplashActivity.this);
        if (!SharedPrefernce.getInstance().getString(Constants.USERNAME).isEmpty()) {

            tvWelcomeMessage.setText(String.format(getString(R.string.welcome_back), SharedPrefernce.getInstance().getString(Constants.USERNAME)));
            bSubmit.setVisibility(View.GONE);
            evUserName.setVisibility(View.GONE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startTopicsActivity();
                }
            }, SPLASH_TIME_OUT);
        }

    }

    private void startTopicsActivity()

    {
        startActivity(new Intent(SplashActivity.this, TopicsActivity.class));
        finish();
    }
}
